// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAbRhWcfn-AIa4ztihp_MmISydbqo451Qo",
    authDomain: "timejob-5cb53.firebaseapp.com",
    databaseURL: "https://timejob-5cb53.firebaseio.com",
    projectId: "timejob-5cb53",
    storageBucket: "timejob-5cb53.appspot.com",
    messagingSenderId: "247187594646",
    appId: "1:247187594646:web:93db14d091d0525d5f11b7",
    measurementId: "G-ZTH8HD3WND"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
