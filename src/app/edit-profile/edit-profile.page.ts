import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular';

import { CrudService } from '../services/profile.service';



@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  profile: any;
  profileNama: string;
  profileTTL: string;
  profileAlamat: string;
  profilePdT: string;
  profileHP: number;


  constructor(
    private navCtrl: NavController,
    private crudService: CrudService
    
  ) { }

  ngOnInit() {

    this.crudService.read_Profile().subscribe(data => {
      this.profile= data.map(e => {
        return { 
          id: e.payload.doc.id,
          isEdit: false,
          Nama: e.payload.doc.data()['Nama'],
          TTL: e.payload.doc.data()['TTL'],
          Alamat: e.payload.doc.data()['Alamat'],
          PdT: e.payload.doc.data()['PdT'],
          HP: e.payload.doc.data()['HP'],
        };
      })

      console.log(this.profile);
    });
  
  }

  

  RemoveRecord(rowID) {
    this.crudService.delete_Profile(rowID);
  }
 
  EditRecord(record) {
    record.isEdit = true;
    record.EditNama = record.Nama;
    record.EditTTL = record.TTL;
    record.EditAlamat = record.Alamat;
    record.EditPdT = record.PdT;
    record.EditHP = record.HP;
  }

  UpdateRecord(recordRow) {
    let record = {};
    record['Nama'] = recordRow.EditNama;
    record['TTL'] = recordRow.EditTTL;
    record['Alamat'] = recordRow.EditAlamat;
    record['PdT'] = recordRow.EditPdT;
    record['HP'] = recordRow.EditHP;
    this.navCtrl.navigateBack('/tabs');
    this.crudService.update_Profile(recordRow.id, record);
    recordRow.isEdit = false;
  }

}
