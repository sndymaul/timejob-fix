import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { CrudService } from '../services/profile.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  profile: any;
  
  profileNama: string;
  profileTTL: string;
  profileAlamat: string;
  profilePdT: string;
  profileHP: number;


  constructor(
  private navCtrl: NavController,
  private crudService: CrudService,
  private authService: AuthenticationService,
    private formBuilder: FormBuilder
    ) {}

    ngOnInit() {

      this.crudService.read_Profile().subscribe(data => {
        this.profile= data.map(e => {
          return { 
            id: e.payload.doc.id,
            isEdit: false,
            Nama: e.payload.doc.data()['Nama'],
            TTL: e.payload.doc.data()['TTL'],
            Alamat: e.payload.doc.data()['Alamat'],
            PdT: e.payload.doc.data()['PdT'],
            HP: e.payload.doc.data()['HP'],
          };
        })
  
        console.log(this.profile);
      });
    }

    


    logout(){
      this.authService.logoutUser()
      .then(res =>{
      console.log(res);
      this.navCtrl.navigateBack('');
      })
      .catch(error => {
      console.log(error);
      })
    }
     

    edit(){
      
      this.navCtrl.navigateBack('/edit-profile');
      
    }

    create(){
      
      this.navCtrl.navigateBack('/create-profile');
      
    }

}
