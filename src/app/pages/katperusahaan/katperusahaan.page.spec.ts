import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KatperusahaanPage } from './katperusahaan.page';

describe('KatperusahaanPage', () => {
  let component: KatperusahaanPage;
  let fixture: ComponentFixture<KatperusahaanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KatperusahaanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KatperusahaanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
