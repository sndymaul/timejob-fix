import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KatperusahaanPageRoutingModule } from './katperusahaan-routing.module';

import { KatperusahaanPage } from './katperusahaan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KatperusahaanPageRoutingModule
  ],
  declarations: [KatperusahaanPage]
})
export class KatperusahaanPageModule {}
