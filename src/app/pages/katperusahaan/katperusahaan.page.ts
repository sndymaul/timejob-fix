import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-katperusahaan',
  templateUrl: './katperusahaan.page.html',
  styleUrls: ['./katperusahaan.page.scss'],
})
export class KatperusahaanPage implements OnInit {

  perusahaan: Observable<any[]>;

  
  constructor(private router: Router, private afs: AngularFirestore) { 
  this.perusahaan = afs.collection('perusahaan').valueChanges();
  }

  ngOnInit() {
  }

  click(){
  	this.router.navigate(['/tabs/tab2']);
  }

}
