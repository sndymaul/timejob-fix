import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KatperusahaanPage } from './katperusahaan.page';

const routes: Routes = [
  {
    path: '',
    component: KatperusahaanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KatperusahaanPageRoutingModule {}
