import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IsirestoranPageRoutingModule } from './isirestoran-routing.module';

import { IsirestoranPage } from './isirestoran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IsirestoranPageRoutingModule
  ],
  declarations: [IsirestoranPage]
})
export class IsirestoranPageModule {}
