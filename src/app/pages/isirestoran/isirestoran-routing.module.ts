import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsirestoranPage } from './isirestoran.page';

const routes: Routes = [
  {
    path: '',
    component: IsirestoranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IsirestoranPageRoutingModule {}
