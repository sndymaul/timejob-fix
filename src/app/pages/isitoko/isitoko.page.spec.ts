import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IsitokoPage } from './isitoko.page';

describe('IsitokoPage', () => {
  let component: IsitokoPage;
  let fixture: ComponentFixture<IsitokoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsitokoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IsitokoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
