import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-isitoko',
  templateUrl: './isitoko.page.html',
  styleUrls: ['./isitoko.page.scss'],
})
export class IsitokoPage implements OnInit {

 constructor(public alertCtrl: AlertController) { }
  async showConfirm() {  
    const confirm = await this.alertCtrl.create({  
      header: 'Konfirmasi!',  
      message: 'Permohonan Anda Terkirim',  
      buttons: [    
        {  
          text: 'Oke',  
          handler: () => {  
            console.log('Confirm Okay.');  
          }  
        }  
      ]  
    });  
    await confirm.present();  
  }  

  ngOnInit() {
  }

}
