import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsitokoPage } from './isitoko.page';

const routes: Routes = [
  {
    path: '',
    component: IsitokoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IsitokoPageRoutingModule {}
