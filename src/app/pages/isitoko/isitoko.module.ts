import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IsitokoPageRoutingModule } from './isitoko-routing.module';

import { IsitokoPage } from './isitoko.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IsitokoPageRoutingModule
  ],
  declarations: [IsitokoPage]
})
export class IsitokoPageModule {}
