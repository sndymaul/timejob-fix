import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsipopulerPage } from './isipopuler.page';

const routes: Routes = [
  {
    path: '',
    component: IsipopulerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IsipopulerPageRoutingModule {}
