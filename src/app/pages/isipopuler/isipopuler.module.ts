import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IsipopulerPageRoutingModule } from './isipopuler-routing.module';

import { IsipopulerPage } from './isipopuler.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IsipopulerPageRoutingModule
  ],
  declarations: [IsipopulerPage]
})
export class IsipopulerPageModule {}
