import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-kattoko',
  templateUrl: './kattoko.page.html',
  styleUrls: ['./kattoko.page.scss'],
})
export class KattokoPage implements OnInit {

  toko: Observable<any[]>;

  constructor(private router: Router, private afs: AngularFirestore) { 
  	this.toko = afs.collection('toko').valueChanges();
  }

  ngOnInit() {
  }

  click(){
  	this.router.navigate(['/tabs/tab2']);
  }

}
