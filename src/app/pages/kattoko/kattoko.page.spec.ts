import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KattokoPage } from './kattoko.page';

describe('KattokoPage', () => {
  let component: KattokoPage;
  let fixture: ComponentFixture<KattokoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KattokoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KattokoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
