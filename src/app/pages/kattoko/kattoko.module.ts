import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KattokoPageRoutingModule } from './kattoko-routing.module';

import { KattokoPage } from './kattoko.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KattokoPageRoutingModule
  ],
  declarations: [KattokoPage]
})
export class KattokoPageModule {}
