import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsiperusahaanPage } from './isiperusahaan.page';

const routes: Routes = [
  {
    path: '',
    component: IsiperusahaanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IsiperusahaanPageRoutingModule {}
