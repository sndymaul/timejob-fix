import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IsiperusahaanPage } from './isiperusahaan.page';

describe('IsiperusahaanPage', () => {
  let component: IsiperusahaanPage;
  let fixture: ComponentFixture<IsiperusahaanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IsiperusahaanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IsiperusahaanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
