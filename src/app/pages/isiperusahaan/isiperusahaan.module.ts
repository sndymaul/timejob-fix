import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IsiperusahaanPageRoutingModule } from './isiperusahaan-routing.module';

import { IsiperusahaanPage } from './isiperusahaan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IsiperusahaanPageRoutingModule
  ],
  declarations: [IsiperusahaanPage]
})
export class IsiperusahaanPageModule {}
