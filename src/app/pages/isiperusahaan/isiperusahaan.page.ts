import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular'; 

@Component({
  selector: 'app-isiperusahaan',
  templateUrl: './isiperusahaan.page.html',
  styleUrls: ['./isiperusahaan.page.scss'],
})
export class IsiperusahaanPage implements OnInit {

  constructor(public alertCtrl: AlertController) { }
	async showConfirm() {  
    const confirm = await this.alertCtrl.create({  
      header: 'Konfirmasi!',  
      message: 'Permohonan Anda Terkirim',  
      buttons: [    
        {  
          text: 'Oke',  
          handler: () => {  
            console.log('Confirm Okay.');  
          }  
        }  
      ]  
    });  
    await confirm.present();  
  }  

  ngOnInit() {
  }

}
