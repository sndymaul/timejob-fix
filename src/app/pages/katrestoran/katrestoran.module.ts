import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KatrestoranPageRoutingModule } from './katrestoran-routing.module';

import { KatrestoranPage } from './katrestoran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KatrestoranPageRoutingModule
  ],
  declarations: [KatrestoranPage]
})
export class KatrestoranPageModule {}
