import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KatrestoranPage } from './katrestoran.page';

const routes: Routes = [
  {
    path: '',
    component: KatrestoranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KatrestoranPageRoutingModule {}
