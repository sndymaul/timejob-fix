import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-katrestoran',
  templateUrl: './katrestoran.page.html',
  styleUrls: ['./katrestoran.page.scss'],
})
export class KatrestoranPage implements OnInit {

restoran: Observable<any[]>;

  constructor(private router: Router, private afs: AngularFirestore) {
  		this.restoran = afs.collection('restoran').valueChanges();
  }

  ngOnInit() {
  }

    click(){
  	this.router.navigate(['/tabs/tab2']);
  }

}
