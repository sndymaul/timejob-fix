import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


 
@Injectable({
  providedIn: 'root'
})
export class CrudService {
 
  constructor(
    private firestore: AngularFirestore,
   
  ) { }
 
 
  create_NewProfile(record) {
    return this.firestore.collection('Profile').add(record);
  }
 
  read_Profile() {
    return this.firestore.collection('Profile').snapshotChanges();
  }
 
  update_Profile(recordID,record){
    this.firestore.doc('Profile/' + recordID).update(record);
  }
 
  delete_Profile(record_id) {
    this.firestore.doc('Profile/' + record_id).delete();
  }
  
}