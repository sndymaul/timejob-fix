import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeskripsiInboxPageRoutingModule } from './deskripsi-inbox-routing.module';

import { DeskripsiInboxPage } from './deskripsi-inbox.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeskripsiInboxPageRoutingModule
  ],
  declarations: [DeskripsiInboxPage]
})
export class DeskripsiInboxPageModule {}
