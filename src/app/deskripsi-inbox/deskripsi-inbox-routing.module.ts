import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeskripsiInboxPage } from './deskripsi-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: DeskripsiInboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeskripsiInboxPageRoutingModule {}
