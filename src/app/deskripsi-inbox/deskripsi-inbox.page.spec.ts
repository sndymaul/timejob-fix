import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeskripsiInboxPage } from './deskripsi-inbox.page';

describe('DeskripsiInboxPage', () => {
  let component: DeskripsiInboxPage;
  let fixture: ComponentFixture<DeskripsiInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeskripsiInboxPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeskripsiInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
