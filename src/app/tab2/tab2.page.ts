import { Component } from '@angular/core';
import {IonSlides } from '@ionic/angular';  
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page{


  items: Observable<any[]>;
  constructor(db: AngularFirestore) {
    this.items = db.collection('semua').valueChanges();
    }

}
