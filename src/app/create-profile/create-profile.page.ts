import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CrudService } from '../services/profile.service';
import { File } from '@ionic-native/file/ngx';
import { FileChooser } from '@ionic-native/file-chooser/ngx';




@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.page.html',
  styleUrls: ['./create-profile.page.scss'],
})
export class CreateProfilePage implements OnInit {

  profile: any;
  profileNama: string;
  profileTTL: string;
  profileAlamat: string;
  profilePdT: string;
  profileHP: number;


  constructor(
    private navCtrl: NavController,
    private crudService: CrudService,
    private file: File,
    private fileChooser: FileChooser
   
  ) { }

  ngOnInit() {

    this.crudService.read_Profile().subscribe(data => {
      this.profile= data.map(e => {
        return { 
          id: e.payload.doc.id,
          isEdit: false,
          Nama: e.payload.doc.data()['Nama'],
          TTL: e.payload.doc.data()['TTL'],
          Alamat: e.payload.doc.data()['Alamat'],
          PdT: e.payload.doc.data()['PdT'],
          HP: e.payload.doc.data()['HP'],
        };
      })

      console.log(this.profile);
    });
  }

  CreateRecord() {
    let record = {};
    record['Nama'] = this.profileNama;
    record['TTL'] = this.profileTTL;
    record['Alamat'] = this.profileAlamat;
    record['PdT'] = this.profilePdT;
    record['HP'] = this.profileHP;
    this.navCtrl.navigateBack('/tabs');
    this.crudService.create_NewProfile(record).then(resp => {
      this.profileNama = "";
      this.profileTTL = "";
      this.profileAlamat = "";
      this.profilePdT = "";
      this.profileHP = undefined;

      console.log(resp);
    })
    .catch(error => {
      console.log(error);
    });
  }

  



}
